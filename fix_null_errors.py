with open("sms_components.csv", 'rb') as open_file:
    text = open_file.read()
    text = text.replace('\x00', "")
    text = text.replace('\xff', "")

    with open("replaced_sms_components.csv", 'wb') as new_file:
        new_file.write(text)