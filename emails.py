import matplotlib.pyplot as plt
import matplotlib.dates as mdates   
from matplotlib.ticker import MultipleLocator, AutoMinorLocator, FuncFormatter, LinearLocator
# import numpy as np
import requests
import os
from jinja2 import Template
# from interface import return_graph_seven_days
import interface
import comms_logging
import math
# from wand.image import Image
import tempfile
import subprocess
from debug import dlog as debug_log
from datetime import datetime
import codecs
import os
import time

API_KEY = "key-4d8bcdb9a9f5c5b0106bb94cb555be93"
# DOMAIN = "sandboxd873e86e9cae4e04a46ccd9be90d4ad1.mailgun.org"
DOMAIN = "mg.smokefreehomes.network"


def make_and_send_email(participant_id, email, seven_day_average, local_sfh, graph_filename, template="email_template.html"):
    # graph_filename = interface.return_graph_seven_days(participant_id)
    try:
        debug_log("in make_and_send_email")
    except Exception as e:
        print e
        debug_log(e)
        graph_filename = "none.jpg"
    print "graph_filename"
    print graph_filename
    average_viz = get_average_viz(seven_day_average)
    average_viz_filename = os.path.split(average_viz)[-1]
    # average_viz_filename = average_viz
    print average_viz
    print average_viz_filename
    print "moving on"
    content = get_email_content(participant_id, seven_day_average, local_sfh, graph_filename, average_viz, template)
    status = send_email(content, email, [graph_filename, average_viz_filename])
    try:
        day = interface.get_participant_day(participant_id)
    except:
        day = 0
    try:
        comms_logging.log(participant_id, "email", str(day), content)
    except Exception as e:
        debug_log(e)
    print status


def get_email_content(participant_id, seven_day_average, local_sfh, graph_filename, average_viz_filename, template="email_template.html"):
    report_template = Template(codecs.open(template, 'rb', encoding="utf-8").read())

    html = report_template.render(avg_value=seven_day_average, local_sfh=local_sfh, graph="cid:"+graph_filename, participant=participant_id, average_viz="cid:" + average_viz_filename, current_time=datetime.now().strftime("%d/%m/%y %H:%M"))
    # print html
    return html


def send_email(content, email, files=[]):
    # print os.path.abspath(files[0])
    file_data = []
    print "in send_email"
    print files
    for i in files:
        try:
            print i
            thisfile = open(i, 'rb')
            file_data.append(("inline", thisfile))
            print thisfile
        except IOError as e:
            print e
            print "File not found, ignoring..."
    print file_data
    return requests.post("https://api.mailgun.net/v3/" + DOMAIN + "/messages", auth=("api", API_KEY), files=file_data, data={"from": "TackSHS <noreply@smokefreehomes.network>", "to": [email], "subject": "Information about your air", "html": content})


def line_graph_PM25(datetimes, pm_records, local_smokefree_pm, dpi=150, filename="figure.png"):
    # """
    # This is optimistically named.
    # """
    # debug_log("Better graph, dicts type: ", type(dicts), " dicts[0] type: ", type(dicts[0]))
    # Stylesheet
    # plt.style.use("afresh.mplstyle")

    # x axis shows a series of datetimes
    x_axis = datetimes

    plt.xlabel("Time")

    # y axis shows PM data
    y_axis = pm_records

    # Set figure, figure size
    fig = plt.figure(figsize=(6, 4))

    plt.plot_date(x_axis,
             y_axis,
             "b-",
             None,
             )
    plt.plot(x_axis,
             [local_smokefree_pm for x in datetimes],
             "r--",
             label="Average smoke-free home")

    ax = plt.gca()  # Get current axes

    plt.xticks(rotation="horizontal")

    loc = mdates.HourLocator(interval=12)
    ax.xaxis.set_major_locator(loc)
    fmt = mdates.DateFormatter("%I%p")
    print "**************************"
    print fmt
    print "**************************"
    ax.xaxis.set_major_formatter(fmt)

    min_loc = mdates.HourLocator(interval=24)
    ax.xaxis.set_minor_locator(min_loc)
    min_fmt = mdates.DateFormatter("%d/%m")
    ax.xaxis.set_minor_formatter(min_fmt)

    ax.tick_params(direction="out", pad=15)

    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(8)
    # fig = plt.figure()
    # fig.autofmt_xdate()

    plt.tight_layout()
    filename = filename.replace(" ", "")
    # filename = os.path.join(tempfile.gettempdir(), filename)
    plt.savefig(filename, format="png", dpi=dpi)
    plt.clf()
    debug_log("graph written to " + filename)
    return filename


def get_average_viz(pm25_value):
    pm25_value = float(pm25_value)
    pixels_to_cm = 35.43307 # Magic SVG cm...
    avg_over = str(int(math.floor(float(pm25_value)/25.00)))
    if get_abd_aqi_descriptions(pm25_value) == "good":
        bubble_x = 1.00
    elif get_abd_aqi_descriptions(pm25_value) == "moderate":
        bubble_x = 6.00
    elif get_abd_aqi_descriptions(pm25_value) == "unhealthy":
        bubble_x = 16.00
    elif get_abd_aqi_descriptions(pm25_value) == "hazardous":
        bubble_x = 21.00
    svg_start = """
    <svg width="35cm" height="20cm">
    <g>
    <rect width="5cm" height="15cm" fill="green"></rect>
    <text x="1.2cm" y="14cm" fill="white" font-family="Calibri" font-size="40">Good</text>
    </g>
    <g>
    <rect width="5cm" height="15cm" x="5cm" fill="#eeee22"></rect>
    <text x="5.2cm" y="14cm" fill="black" font-family="Calibri" font-size="40">Moderate</text>
    </g>
    <g>
    <rect width="10cm" height="15cm" x="10cm" fill="red"></rect>
    <text x="12.7cm" y="14cm" fill="white" font-family="Calibri" font-size="40">Unhealthy</text>
    </g>
    <g>
    <rect width="5cm" height="15cm" x="20cm" fill="#900000"></rect>
    <text x="20.2cm" y="14cm" fill="white" font-family="Calibri" font-size="40">Hazardous</text>
    </g>
    <rect width="40cm" height="0.5cm" x="0cm" y="12cm" fill="white"></rect>
    """
    debug_log(bubble_x)
    if pm25_value > 25:
        bubble = \
        """
        <polygon points="0,0 0,4, 1,4 1.5,5 2,4 8,4, 8,0" fill="white" style='stroke:black;stroke-width:0.03;' transform="translate(%f,100) scale(35.43307)"></polygon>
        <text x='%f' y='175' font-size='72' fill='%s' font-family='Calibri' font-weight='bold'>%d</text>
        <text x='%f' y='125' font-size='20' fill='black' font-family='Calibri Light'>More than %s
        <tspan x='%f' y='150'>times the</tspan>
        <tspan x='%f' y='175'>recommended limit</tspan></text>
        """ % (bubble_x*pixels_to_cm, bubble_x*pixels_to_cm+1.5, get_abd_aqi_colours(pm25_value), int(pm25_value), (3.5+bubble_x)*pixels_to_cm, avg_over, (3.5+bubble_x)*pixels_to_cm, (3.5+bubble_x)*pixels_to_cm)
    else:
        bubble = \
        """
        <polygon points="0,0 0,4, 1,4 1.5,5 2,4 8,4, 8,0" fill="white" style='stroke:black;stroke-width:0.03;' transform="translate(%f,100) scale(35.43307)"></polygon>
        <text x='%f' y='175' font-size='72' fill='%s' font-family='Calibri' font-weight='bold'>%d</text>
        <text x='%f' y='150' font-size='20' fill='black' font-family='Calibri Light'>Less than the
        <tspan x='%f' y='175'>recommended limit</tspan></text>
        """ % (bubble_x*pixels_to_cm, bubble_x*pixels_to_cm+1.5, get_abd_aqi_colours(pm25_value), int(pm25_value), (3.5+bubble_x)*pixels_to_cm, (3.5+bubble_x)*pixels_to_cm)
        
        # bubble = "    <g><rect width='7cm' height='4cm' y='5cm' x='" + str(bubble_x) + "cm' fill='white' style='stroke:black;stroke-width:0.7;'></rect><text x='" + str(bubble_x+0.5) + "cm' y='7.5cm' font-size='72' fill='" + get_abd_aqi_colours(pm25_value) + "' font-family='Calibri' font-weight='bold'>400</text></g>"
    svg_end = "</svg>"
    all_svg = svg_start + bubble + svg_end
    temp_out = os.path.join(tempfile.gettempdir(), "temp_out.svg")
    # temp_out = "temp_out.svg"
    with open(temp_out, 'wb') as svg_file:
        svg_file.write(all_svg)
    # out_file = os.path.join(tempfile.gettempdir(), "temp_out.png")
    print temp_out
    # out_file = os.path.join(tempfile.gettempdir(), "temp_out.png")
    out_file = "temp_out.png"
    print out_file
    # out_file = "temp_out.png"
    # print os.getcwd()
    # out_file = os.path.join(os.getcwd(), out_file)
    # time.sleep(5)
    # result = subprocess.call(["convert", temp_out, out_file])
    # print "convert.exe " + temp_out + " " + out_file
    result = os.system("magick convert " + temp_out + " " + out_file)
    # print result
    try:
        debug_log("attempted to convert svg to png with result: " + str(result))
    except:
        debug_log("attempted to log result of attempted svg to png conversion, but there was an error (probably in string conversion)")
    # print out_file
    return out_file


def get_abd_aqi_colours(pm25_value):
    """
    Combination of modified US EPA AQI and WHO guidance daily mean level
    """
    # pm25_value = round(pm25_value)
    if pm25_value <= 25:
        return "green"
    elif pm25_value > 25 and pm25_value <= 55.4:
        return "#eeee22"
    elif pm25_value > 55.4 and pm25_value <= 250.4:
        return "red"
    elif pm25_value > 250.4:
        return "#900000"


def get_abd_aqi_descriptions(pm25_value):
    """
    Combination of modified US EPA AQI and WHO guidance daily mean level
    """
    if pm25_value <= 25:
        return "good"
    elif pm25_value > 25 and pm25_value <= 55.4:
        return "moderate"
    elif pm25_value > 55.4 and pm25_value <= 250.4:
        return "unhealthy"
    elif pm25_value > 250.4:
        return "hazardous"


if __name__ == '__main__':
    aviz = get_average_viz(15)
    make_and_send_email("participant_id", "r.p.dobson@stir.ac.uk", "15", "11", "Greece_test.png")
    # os.system("explorer " + aviz)
    # content = get_email_content("WP4_SCO_02", 20, 10, "None", "avg_viz.png")
    # print content
    # make_and_send_email("WP4_SCO_05", "ruaraidh.dobson@abdn.ac.uk", 10, 15)
    # send_email(content, "ruaraidh.dobson@abdn.ac.uk")
    # "<html><h1 style='font-family:\"Helvetica\", arial, sans-serif'>This is a test</h1><img src='cid:figure.png'></html>" ##, ["figure.png"]
