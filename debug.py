# import appdirs
import datetime


def dlog(err, filename=None):
    now = datetime.datetime.now().strftime("%d/%m/%y %H:%M")
    try:
        message = "\nEVENT: " + now + " " + str(err) + "\n"
    except Exception as e:
        # print e
        dlog("Exception occurred when logging!")
        return False
    print message
    try:
        if filename:
            with open(filename, 'ab') as open_file:
                open_file.write(message)
        else:
            with open("tackshs.log", 'ab') as open_file:
                open_file.write(message)
        return True
    except Exception as e:
        print "Couldn't write log!"
        print e
        return False
