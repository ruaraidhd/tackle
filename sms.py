# -*- coding: utf-8 -*-
from clockwork import clockwork
import sys, os
import codecs
import requests
# import xml.etree.ElementTree as ET
from debug import dlog
import time
import datetime
from twilio.rest import Client

# sys.setdefaultencoding('utf8')

API_KEY = "d9cabdb3fda836e5f0648132d4c8e7e11f36e33c"


def send(to_no, message):
    # try:
    #     api = clockwork.API(API_KEY, from_name="TackSHS")
    #     message_object = clockwork.SMS(to=to_no, message=message)
    #     response = api.send(message_object)
    # except UnicodeDecodeError as e:
    #     dlog("UnicodeDecodeError in clockwork SMS sending block")


    result = False
    # try:
    #     if response.success:
    #         result = True
    #         dlog("Successfully delivered text with clockwork")
    #     else:
    #         result = False
    # except:
    #     result = False
    if result is False:
        try:
            dlog("clockwork failed, trying twilio")
            response = twilio_send(to_no, message)
            # print response.success
            result = True
            dlog("twilio success!")
        except:
            dlog("both clockwork and twilio failed")
            result = False
    return result
    # message = clockwork.SMS(to=to_no, message=message)
    # response = api.send(message)
    # print response
    # if response.success:
    #     return True
    # else:
    #     print response.error_code
    #     print response.error_message
    #     return False

def twilio_send(to_no, message):
    account_sid = "AC172b9feac2e9924d153a61405c40ab6d"
    auth = "868c82f3333658ede46d4c5796220d4a"
    client = Client(account_sid, auth)
    message = client.messages.create(to="+"+to_no, from_="+447481345978", body=message)
    return message


def get_balance():
    api = clockwork.API(API_KEY, from_name="TackSHS")
    balance = api.get_balance()
    return balance

if __name__ == '__main__':

    # message = u"Η τιμή αυτή είναι σχεδόν η ίδια με το μέσο όρο των προηγούμενων επτά ημερών. Η τιμή αυτή είναι σχεδόν η ίδια με το μέσο όρο των προηγούμενων επτά ημερών. Η τιμή αυτή είναι σχεδόν η ίδια με το μέσο όρο των προηγούμενων επτά ημερών."
    message = u"Τα επίπεδα του παθητικού καπνίσματος στο σπίτι σας ήταν 0.0 τις τελευταίες 24 ώρες. Η τιμή αυτή είναι μικρότερη από το μέσο όρο των προηγούμενων επτά ημερών. Μπράβο σας! Η τιμή αυτή είναι περίπου η ίδια με ένα σπίτι που δεν καπνίζουν στην περιοχή σας, Edinburgh. Εκμεταλλευτείτε τις δουλειές εκτός σπιτιού για να καπνίσετε."
    # message = u"τιμή αυτή είναι μικρότερη από το μέσο όρο των προηγούμενων επτά ημερών. Μπράβο σας! Η τιμή αυτή είναι περίπου η ίδια με ένα σπίτι που δεν καπνίζουν στην περιοχή σας, Edinburgh. Εκμεταλλευτείτε τις δουλειές εκτός σπιτιού για να καπνίσετε."
    # message = u"9: ?μεταλλευτείτε τις δουλειές εκτ 10: ός σπιτιού για να καπνίσετε."
    # message = u"4: δια με το μέσο όρο των προηγούμενων ?"
    # message = u"1111111111111111111111111111111111111111111111111122222222222222222222222222222222222222222222222222333333333333333333333333333333333333333333333333334444444444444444444444444444444444444444444444444455555555555555555555555555555555555555555555555555666666666666666666666666666666666666666666666666667777777777777777777777777777777777777"
    # message = "Test"
    message = codecs.encode(message, "utf-8")
    number  = "447803406343"
    with open("writing_message_260118.txt", 'wb') as open_file:
        open_file.write(message)
    # sys.exit()
    # print send(number, message)
    print send(number, message)
    print get_balance()
