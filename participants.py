import csv
import datetime
import os


# class Participant:
#     def __init__(self, participant_id, name, phone, email, address, postcode, startdate, foobot_id):
#         self.participant_id = participant_id
#         self.name = name
#         self.phone = phone
#         self.email = email
#         self.address = address
#         self.postcode = postcode
#         self.startdate = startdate
#         self.foobot_id = foobot_id

#     def __repr__(self):
#         return self.participant_id

def append_participant_to_csv(filename, participant_id, name, phone, email, address, postcode, dylos_id, startdate, enddate, isfinished):
    with open(filename, 'ab') as open_file:
        writer = csv.writer(open_file)
        print "dylos_id"
        print dylos_id
        if str(phone).startswith("\""):
            pass
        else:
            phone = "\"" + str(phone) + "\""
        # writer = csv.DictWriter(open_file, ["participant_id", "name", "phone", "email", "address", "postcode", "foobot_id", "startdate", "enddate", "isfinished"])
        data_dict = {"participant_id":participant_id, "name":name, "phone":phone, "email":email, "address":address, "postcode":postcode, "dylos_id":dylos_id, "startdate":startdate, "enddate":enddate, "isfinished":isfinished}
        writer.writerow([participant_id, name, phone, email, address, postcode, startdate, dylos_id, enddate, isfinished])
        # writer.writerow(data_dict)
    return True

def read_participants_from_csv(filename):
    try:
        with open(filename, 'rb') as open_file:
            reader = csv.DictReader(open_file)
            participants = []
            for row in reader:
                participants.append([row["participant_id"], row["name"], row["phone"].replace("'",""), row["email"], row["address"], row["postcode"], row["dylos_id"], row["startdate"], row["enddate"], row["isfinished"]])
                # print row["name"]
                # print row["startdate"]
            return participants
    except IOError as e:
        try:
            with open(filename, 'wb') as open_file:
                open_file.write("participant_id,name,phone,email,address,postcode,startdate,dylos_id,enddate,isfinished\n")
                return []
        except IOError as e:
            os.makedirs(os.path.split(filename)[0])
            with open(filename, 'wb') as open_file:
                open_file.write("participant_id,name,phone,email,address,postcode,startdate,dylos_id,enddate,isfinished\n")
                return []

if __name__ == '__main__':
    read_participants_from_csv("participant_data.csv")
