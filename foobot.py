import requests
from requests.compat import quote_plus
import time
import csv


# USERNAME = "sean.semple@abdn.ac.uk"
# API_SECRET = "eyJhbGciOiJIUzI1NiJ9.eyJncmFudGVlIjoic2Vhbi5zZW1wbGVAYWJkbi5hYy51ayIsImlhdCI6MTQ3ODcwOTA4OCwidmFsaWRpdHkiOi0xLCJqdGkiOiIwMTYzNmU1ZS1mYjQ4LTRmZjQtODIzOS1lOWI0MjBmNzFkMzEiLCJwZXJtaXNzaW9ucyI6WyJ1c2VyOnJlYWQiLCJkZXZpY2U6cmVhZCJdLCJxdW90YSI6MjAwLCJyYXRlTGltaXQiOjV9.-8BVL8OWLaLbKTL5ksFF0fLmR7O-9ENKU9EV7nRl5ZM"

USERNAME = "seansemple.home@gmail.com"
API_SECRET = "eyJhbGciOiJIUzI1NiJ9.eyJncmFudGVlIjoic2VhbnNlbXBsZS5ob21lQGdtYWlsLmNvbSIsImlhdCI6MTQ3ODg2Mzc4MiwidmFsaWRpdHkiOi0xLCJqdGkiOiJhYzRhOGNlYS1kZmExLTQ0ZjMtYWRhNy03ZDI0NjRkZDYyOWUiLCJwZXJtaXNzaW9ucyI6WyJ1c2VyOnJlYWQiLCJkZXZpY2U6cmVhZCJdLCJxdW90YSI6MjAwLCJyYXRlTGltaXQiOjV9._S3RpT23Wik7B3aaPawJY_uavNTNQPJn6EJ3u4ALotA"


def get_device_list(user=USERNAME, api=API_SECRET):
    # print user
    base_url = "http://api.foobot.io/v2/owner/" + quote_plus(user) + "/device/"
    # print base_url
    headers = {"X-API-KEY-TOKEN": api}
    r = requests.get(base_url, headers=headers)
    # print r
    return r


def get_data(device_uuid, seconds=3628800, data_format="json", user=USERNAME, api=API_SECRET):
    base_url = "http://api.foobot.io/v2/device/" + device_uuid + "/datapoint/" + str(seconds) + "/last/0/"
    headers = {"X-API-KEY-TOKEN": api}
    print "Downloading..."
    if data_format == "csv":
        headers["Accept"] = "text/csv;charset=UTF-8"
    r = requests.get(base_url, headers=headers)
    return r


def days_to_seconds(days):
    return 86400*days


def seconds_to_days(seconds):
    return seconds/86400.00


def get_pretty_date_from_timestamp(timestamp):
    current = time.localtime(timestamp)
    return time.strftime("%d/%m/%y %H:%M", current)


def get_datetime_from_timestamp(timestamp):
    return time.localtime(timestamp)

def write_json_to_csv(json, filename="downloaded_json.csv"):
    pass

if __name__ == '__main__':
    dev_list = get_device_list()
    print dev_list.json()
    if dev_list.status_code == 200:
        print "200: Data successfully retrieved"
    else:
        print dev_list.status_code
        # print "Data retrieval unsuccesful"
        try:
            print dev_list.json()["message"]
            if "stack" in dev_list.json().keys():
                print dev_list.json()["stack"]
        except:
            print "Error: no message in response"
        # exit()
    for monitor in dev_list.json():
        all_data = get_data(monitor["uuid"], days_to_seconds(30), data_format="json").json()
        # print all_data["sensors"]
        # print all_data["units"]
        print monitor
        # exit()
        start = time.localtime(all_data["start"])
        print time.strftime("%d/%m/%y %H:%M", start)
        rows = [["Date", "Time", "Datetime", "PM", "Temperature", "RH", "CO2", "VOC", "global"]]
        for datapoint in all_data["datapoints"]:
            # current = time.localtime(datapoint[0])
            # print get_pretty_date_from_timestamp(datapoint[0])
            # print "PM: " + str(round(datapoint[1]))
            row = datapoint
            date_time = get_pretty_date_from_timestamp(row[0]).split(" ")
            date_time.append(get_pretty_date_from_timestamp(row[0]))
            row.pop(0)
            row = date_time + row
            # print row
            rows.append(row)
        with open("test_csv_150217_2" + monitor["name"] + ".csv", 'wb') as test_csv:
            writer = csv.writer(test_csv)
            writer.writerows(rows)

