import csv
import datetime
import time

def splitter(reader):
    data = [[]]
    for row in reader:
        data[-1].append(row)
        if len(data[-1]) == 10:
            data.append([])
    return data


def averager(data):
    meaned_data = []
    for group in data:
        average = sum([float(x["PM2.5"]) for x in group])/len(group)
        meaned_data.append([group[-1]["Date and time"], average, int(time.mktime(datetime.datetime.strptime(group[-1]["Date and time"], "%d/%m/%Y %H:%M").timetuple()))])
    return meaned_data


if __name__ == '__main__':
    with open("dylos310117.csv", 'rb') as dylos_csv:
        reader = csv.DictReader(dylos_csv)
        splits = splitter(reader)
        averages = averager(splits)
        print averages
        with open("five_minute_average_dylos_310117.csv", 'wb') as fma_file:
            writer = csv.writer(fma_file)
            writer.writerow(["Date and time", "PM2.5", "Timestamp"])
            for each in averages:
                writer.writerow(each)
