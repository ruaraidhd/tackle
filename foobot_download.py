import requests
# import csv
import StringIO

USERNAME = "seansemple.home@gmail.com"
API_KEY = "eyJhbGciOiJIUzI1NiJ9.eyJncmFudGVlIjoic2VhbnNlbXBsZS5ob21lQGdtYWlsLmNvbSIsImlhdCI6MTQ3ODg2Mzc4MiwidmFsaWRpdHkiOi0xLCJqdGkiOiJhYzRhOGNlYS1kZmExLTQ0ZjMtYWRhNy03ZDI0NjRkZDYyOWUiLCJwZXJtaXNzaW9ucyI6WyJ1c2VyOnJlYWQiLCJkZXZpY2U6cmVhZCJdLCJxdW90YSI6MjAwLCJyYXRlTGltaXQiOjV9._S3RpT23Wik7B3aaPawJY_uavNTNQPJn6EJ3u4ALotA"

def get_foobot_devices(user, api):
    r = requests.get("http://api.foobot.io/v2/owner/" + user + "/device/", headers={"X-API-KEY-TOKEN": api})
    return r.json()

def get_foobot_data(uuid, seconds=0):
    r = requests.get("http://api.foobot.io/v2/device/"+ uuid + "/datapoint/" + str(seconds) + "/last/600/", headers={"X-API-KEY-TOKEN": API_KEY, "Accept": "text/csv;charset=UTF-8"})
    return r.text

if __name__ == '__main__':
    ### UNCOMMENT THIS TO DOWNLOAD A CSV OF TACKSHS_1
    # csv_data = get_data("25004667617040D0", 86400*5)
    # with open("foobot_csv3.csv", 'wb') as foocsv:
    #     foocsv.write(csv_data)
    ### END

    devices = get_foobot_devices("seansemple.home@gmail.com", API_KEY)
    for dev in devices:
        uuid = dev["uuid"]
        data = get_foobot_data(uuid, 86400)
        print data
