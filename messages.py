# -*- coding: utf-8 -*-
import csv
import random
import os
import unicodecsv as csv
import codecs
import StringIO

class SMSGenerator:
    def read_sms_csv(self, filename):

        with codecs.open(filename, encoding="utf-8") as open_file:
            text_of_file = open_file.read()
            # print text_of_file
            text_of_file = text_of_file.encode("utf-8")
            open_csv = StringIO.StringIO(text_of_file)
            # with open(file_stringio) as open_csv:
            reader = csv.DictReader(open_csv, delimiter=",")
            self.todaygoingdown = []
            self.todaystable = []
            self.todaygoingup = []
            self.comparisongoingdown = []
            self.comparisonstable = []
            self.comparisongoingup = []
            self.localareahigher = []
            self.localareasameorlower = []
            self.advicegeneral = []
            for row in reader:
                print row.keys()
                if row["Part"] == "Today":
                    if row["Attribute"] == "goingup":
                        self.todaygoingup.append(row["Text"])
                    if row["Attribute"] == "goingdown":
                        self.todaygoingdown.append(row["Text"])
                    if row["Attribute"] == "stable":
                        self.todaystable.append(row["Text"])
                if row["Part"] == "Comparison":
                    if row["Attribute"] == "goingup":
                        self.comparisongoingup.append(row["Text"])
                    if row["Attribute"] == "goingdown":
                        self.comparisongoingdown.append(row["Text"])
                    if row["Attribute"] == "stable":
                        self.comparisonstable.append(row["Text"])
                if row["Part"] == "Localarea":
                    if row["Attribute"] == "higher":
                        self.localareahigher.append(row["Text"])
                    if row["Attribute"] == "sameorlower":
                        self.localareasameorlower.append(row["Text"])
                if row["Part"] == "Advice":
                    if row["Attribute"] == "general":
                        self.advicegeneral.append(row["Text"])
            # print self.todaygoingup
            # print self.advicegeneral


    def generate_sms(self, mean_24hrs, last_seven_days_mean, local_area_sfh, your_city="your city"):
        text = u""
        your_city = unicode(your_city, encoding="utf-8")
        if mean_24hrs > last_seven_days_mean + 10:
            text += random.choice(self.todaygoingup).replace("{{today_result}}", str(mean_24hrs))
            text += " "
            text += random.choice(self.comparisongoingup)
        elif mean_24hrs < last_seven_days_mean - 10:
            text += random.choice(self.todaygoingdown).replace("{{today_result}}", str(mean_24hrs))
            text += " "
            text += random.choice(self.comparisongoingdown)
        else:
            text += random.choice(self.todaystable).replace("{{today_result}}", str(mean_24hrs))
            text += " "
            text += random.choice(self.comparisonstable)
        text += " "
        if mean_24hrs > local_area_sfh + 10:
            text += random.choice(self.localareahigher)
        else:
            text += random.choice(self.localareasameorlower)
        text += " "
        text += random.choice(self.advicegeneral)
        text = text.replace("{{your_city}}", your_city)
        return text

    def __init__(self, filename):
        self.read_sms_csv(filename)



if __name__ == '__main__':
    gen = SMSGenerator("sms_components.csv")
    print gen.generate_sms(random.randint(0,100), random.randint(0,100), random.randint(0,100))