from ConfigParser import SafeConfigParser
import datetime

class Settings:
    participant_data_file = "participant_data.csv"
    local_smokefree_home_pm = 10.00
    last_sync = datetime.datetime.fromordinal(735398)
    username = ""
    apikey = ""
    comms_log_folder = "comms_logs"
    your_city = "BLANK"

    def __init__(self, filename):
        self.filename = filename
        try:
            self.read_settings(filename)
        except:
            self.write_settings()

    def read_settings(self, filename):
        parser = SafeConfigParser()
        parser.read(filename)
        self.participant_data_file = parser.get("participants", "participant_data")
        self.local_smokefree_home_pm = float(parser.get("local", "local_smokefree_home_pm"))
        self.last_sync = datetime.datetime.strptime(parser.get("appdata", "last_synced"), "%d/%m/%y_%H:%M")
        try:
            self.username = parser.get("appdata", "username")
        except:
            print "couldn't find username"
            self.username = ""
        try:
            self.apikey = parser.get("appdata", "apikey")
        except:
            print "couldn't set api key"
            self.apikey = ""
        try:
            self.comms_log_folder = parser.get("participants", "comms_log_folder")
        except:
            print "couldn't get comms log folder, defaulting"
            self.comms_log_folder = "comms_logs"
        try:
            self.your_city = parser.get("local", "your_city")
        except:
            print "couldn't get your_city, defaulting"
            self.your_city = "BLANK"

    def write_settings(self):
        parser = SafeConfigParser()
        parser.add_section("participants")
        parser.add_section("local")
        parser.add_section("appdata")
        parser.set("participants", "participant_data", self.participant_data_file)
        parser.set("participants", "comms_log_folder", self.comms_log_folder)
        parser.set("local", "local_smokefree_home_pm", str(self.local_smokefree_home_pm))
        parser.set("local", "your_city", self.your_city)
        parser.set("appdata", "last_synced", self.last_sync.strftime("%d/%m/%y_%H:%M"))
        parser.set("appdata", "username", self.username)
        parser.set("appdata", "apikey", self.apikey)
        with open(self.filename, 'wb') as settings_file:
            parser.write(settings_file)

if __name__ == '__main__':
    settings = Settings("settings.ini")
    print settings.participant_data_file
