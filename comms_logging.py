import datetime
import os


def get_log(participant_id, comms_log_folder):
    filename = os.path.join(comms_log_folder, participant_id + ".log")
    print "comms_log: ", filename
    try:
        with open(filename, 'rb') as par_log:
            return par_log.read()
    except IOError as e:
        return ""


def log_header(participant_id, comm_type, day):
    now = datetime.datetime.now().strftime("%d/%m/%y %H:%M")
    header = "HEAD: Comm: " + participant_id + " sent " + comm_type + " on day " + str(day) + " at " + now + "\n"
    return header


def parse_log(content):
    sections = content.split("HEAD: ")
    all_data = []
    for each in sections:
        try:
            both = each.split("CONT: ")
            header = both[0]
            comm_type = header.split("sent ")[1].split(" on day ")[0]
            day = header.split("on day ")[1].split(" at ")[0]
            all_data.append((day, comm_type))
        except Exception as e:
            print e
            all_data.append(("", ""))
    return all_data
            # return ""

def log(participant_id, comms_log_folder, comm_type, day, comms_content=""):
    day = str(day)
    filename = os.path.join(comms_log_folder, participant_id + ".log")
    # if exists
    if not os.path.isdir(comms_log_folder):
        os.mkdir(comms_log_folder)
    if os.path.isfile(filename):
        par_log = open(filename, 'ab')
    else:
        par_log = open(filename, 'wb')
    header = log_header(participant_id, comm_type, day)
    comms_content = "CONT: " + comms_content
    header += comms_content + "\n"
    par_log.write(header)
