import sys
import os
import ctypes

from PySide.QtGui import *
from PySide.QtCore import *
from cefpython3 import cefpython
import datetime
import sms
from settings import Settings
import foobot
import participants
# import data_storage
from messages import SMSGenerator
import emails
import comms_logging
import tempfile
from debug import dlog
import FileDialog # required by pyinstaller
import dylos
import codecs

dlog(os.getcwd())

#I can't believe how awful this is, I pray no-one ever sees it.
if "sms_components.csv" not in os.listdir(os.getcwd()):
    script_dir = os.path.dirname(os.path.realpath(__file__))
    dlog(script_dir)
    os.chdir(script_dir)
    os.chdir("..")

all_participants_data = None

# Load app settings, participant data csv etc
settings_file = "settings.ini"
print os.path.isfile(settings_file)
if not os.path.isfile(settings_file):
    dlog("Settings getting started...")
    with open(settings_file, 'wb') as open_file:
        defaults = "[participants]" + os.linesep + "participant_data=participant_data.csv" + os.linesep + "[local]" + os.linesep + "local_smokefree_home_pm=10"
        open_file.write(defaults)
        print "2"
        open_file.close()
app_settings = Settings(settings_file)

sms_gen = SMSGenerator("sms_components.csv") # This should be in the settings.ini

# Load participants
parts = participants.read_participants_from_csv(app_settings.participant_data_file)
# dlog(parts)
dlog("participants loaded")
parts_dict = {}
for participant in parts:
    parts_dict[participant[0]] = participant[1:]


def get_data_for_a_participant(participant_id):
    participants_with_id = [x for x in parts if x[0] == participant_id]
    try:
        participant = participants_with_id[0]
    except IndexError as e:
        dlog("Couldn't find a participant with that ID")
        return None
    dylos_id = participant[6]
    startdate = participant[7]
    try:
        data = dylos.get_records_since_start_of_intervention_and_metadata(dylos_id, startdate)
    except Exception as e:
        dlog("Exception in get_data_for_a_participant: " + str(e))
        return None
    return data


def callback_activate_participant(participant_id, callback):
    data = get_data_for_a_participant(participant_id)
    callback.Call(data)
    return data


def get_sms_draft(participant_id, callback):
    dlog("get_sms_draft")
    # text = sms_gen.generate_sms(data_storage.get_mean_pm_last_day(participant_id),
    #                             data_storage.get_mean_pm_last_seven_days(participant_id),
    #                             app_settings.local_smokefree_home_pm)
    part = all_participants_data[participant_id]
    text = sms_gen.generate_sms(part["mean_last_24_hours"], part["mean_last_seven_days"], app_settings.local_smokefree_home_pm, app_settings.your_city)
    print type(text)
    text = text.encode("utf-8")
    # text = requests.utils.quote(text)
    callback.Call(text)


def send_text_message(number, text, participant_id, day, callback):
    try:
        dlog("sending " + str(text) + " to " + str(number))
    except UnicodeEncodeError:
        dlog("sending message but can't write it here - unicode problems")
    # text = text.decode("utf-8")
    try:
        result = sms.send(number, text)
    except Exception as e:
        dlog(e)
        # dlog(text)
        callback.Call("message not sent")
        return
    print result
    if result == True:
        message = "message sent"
        # text = text.encode("utf-8")
        comms_logging.log(participant_id, app_settings.comms_log_folder, "sms", str(day), text)
    else:
        message = "message not sent"
    callback.Call(message)


def get_all_data_for_all_participants():
    dlog("get_all_data_for_all_participants")
    aborted = False #in case we can't raise a connection
    try:
        parti = participants.read_participants_from_csv(app_settings.participant_data_file)
    except Exception as e:
        print "Exception!", e
        dlog("Exception: " + str(e))
        parti = []
    print "parti"
    print parti
    all_participants = {}
    for i in parti:
        print i
        if i[8] == "":
            i[8] = None
        # try:
        #     print i[6], i[7], i[8]
        try:
            records = dylos.get_records_since_start_of_intervention_and_metadata(i[6], i[7], i[8])
        except Exception as e:
            dlog("Exception" + str(e))
            if "Connection aborted." in e[0]:
                aborted = True
            records = []
        print len(records)
        all_participants[i[0]] = records
        # except Exception as e:
        #     print "\n\n\nOh no!\n\n\n"
        #     print e
    if aborted:
        return False
    else:
        print "all_participants"
        print type(all_participants)
        # print all_participants
        return all_participants



def log_phone_call(participant_id, day, content, callback):
    dlog("log_phone_call")
    comms_logging.log(participant_id, app_settings.comms_log_folder, "phonecall", day, content)
    callback.Call(True)


def get_seven_day_mean(participant_id, callback):
    dlog("get_seven_day_mean")
    # mean = data_storage.get_mean_pm_last_seven_days(participant_id)
    print "participant_id:"
    print participant_id
    print "all_participants_data.keys():"
    print all_participants_data.keys()
    # print all_participants_data
    mean = all_participants_data[participant_id]["mean_last_seven_days"]
    # get_participant_day(participant_id)
    callback.Call(mean)


def get_last_day_mean(participant_id, callback):
    dlog("get_last_day_mean")
    # mean = data_storage.get_mean_pm_last_day(participant_id)
    mean = all_participants_data[participant_id]["mean_last_24_hours"]
    print "\n\n\n\n\n\n"
    print mean
    print "last day's mean"
    print "\n\n\n\n\n\n"
    callback.Call(mean)


def get_local_smokefree_pm(callback):
    dlog("get_local_smokefree_pm")
    local_smokefree = app_settings.local_smokefree_home_pm
    callback.Call(local_smokefree)


def return_graph_since_start_of_intervention(participant_id):
    dlog("return_graph_since_start_of_intervention")
    # parti = participants.read_participants_from_csv(app_settings.participant_data_file)
    # startdate = datetime.strptime("%d/%m/%Y", parti[7])
    data = all_participants_data[participant_id]["data"]
    pm = dylos.get_pm_from_data(data)
    datetimes = dylos.get_datetimes_from_data(data)
    filename = emails.line_graph_PM25(datetimes, pm, 10, filename=participant_id + ".png")
    print filename
    return filename


def get_graph_since_start_of_intervention(participant_id, callback):
    dlog("get_graph_since_start_of_intervention")
    filename = return_graph_since_start_of_intervention(participant_id)
    callback.Call(filename)



def return_graph_seven_days(participant_id):
    dlog("return_graph_seven_days")
    # records = data_storage.get_records_last_seven_days(participant_id)
    # data = all_participants_data[participant_id]["data"][0:10080] # 60*24*7 - THIS GETS THE LAST SEVEN DAYS, NOT RECORDS *FROM* THE LAST SEVEN DAYS!
    data = all_participants_data[participant_id]["last_seven_days"]
    pm = dylos.get_pm_from_data(data)
    datetimes = dylos.get_datetimes_from_data(data)
    filename = emails.line_graph_PM25(datetimes, pm, 10, filename=participant_id + ".png")
    print filename
    return filename

    # try:
    #     print records[0].participant
    # except IndexError as e:
    #     print e
    #     dlog("No records returned")
    # pm_records = [x.PM25 for x in records]
    # dates = [x.datetime for x in records]
    # filename = emails.line_graph_PM25(dates, pm_records, 10, filename=participant_id + ".png")
    # return filename


def get_graph_seven_days(participant_id, callback):
    dlog("get_graph_seven_days")
    filename = return_graph_seven_days(participant_id)
    callback.Call(filename)


def participant_id_to_dylos_id(participant_id):
    dlog("participant_id_to_dylos_id")
    parti = participants.read_participants_from_csv(app_settings.participant_data_file)
    with [{x["participant_id"]: x["dylos_id"]} for x in parti] as parti_list:
        try:
            return parti_list[participant_id]
        except KeyError as e:
            print e
            return None


def participant_id_to_startdate(participant_id):
    dlog("participant_id_to_startdate")
    parti = participants.read_participants_from_csv(app_settings.participant_data_file)
    with [{x["participant_id"]: x["startdate"]} for x in parti] as parti_list:
        try:
            return parti_list[participant_id]
        except KeyError as e:
            print e
            return None


def get_foobot_name_uuid_dict():
    """
    Returns a dictionary of names: uuids of monitors
    """
    dlog("get_foobot_name_uuid_dict")
    devices = foobot.get_device_list(user=app_settings.username, api=app_settings.apikey).json()
    name_uuid_dict = {}
    for monitor in foobot.get_device_list().json():
        name_uuid_dict[monitor["name"]] = monitor["uuid"]
    return name_uuid_dict


# I think this is now obsolete, viz data_storage.py
# def download_foobot_data(foobot_id, callback):
#     devices = foobot.get_device_list().json()
#     print devices
#     name_dict = get_foobot_name_uuid_dict()
#     print name_dict[foobot_id]
#     ident = name_dict[foobot_id]
#     foobot_data = foobot.get_data(ident, user=app_settings.username, api=app_settings.apikey).json()
#     # print foobot_data
#     callback.Call(str(foobot_data["datapoints"][0]))


def is_selected(participant_id, callback):
    dlog("is_selected")
    if participant_id:
        callback.Call(True)
    else:
        callback.Call(False)


def get_participant_day(participant_id):
    """
    Doesn't return fractions, so watch out.
    """
    dlog("get_participant_day")
    day = datetime.datetime.strptime(parts_dict[participant_id][6], "%d/%m/%Y")
    difference = datetime.datetime.now() - day
    return difference.days + 1  # +1 because otherwise the first install day would be day 0


def callback_participant_day(participant_id, callback):
    dlog("callback_participant_day")
    part_days = get_participant_day(participant_id)
    callback.Call(part_days)


def callback_participant_required_today(day, callback):
    dlog("callback_participant_required_today")
    day = int(day)
    if day < 0:
        callback.Call("This participant has not yet begun the intervention. Ensure you have given them consent forms and participant information as directed in the protocol, but do not yet message them about their air quality")
    elif day >= 0 and day < 8:
        callback.Call("For the first seven full days, the participant doesn't receive SMS messages or other communication.")
    elif day == 8 or day == 16 or day == 22:
        callback.Call("The participant should receive an SMS message and an email today. You will phone to discuss this tomorrow.")
    elif day == 9 or day == 23:
        callback.Call("The participant should receive an SMS message and a phone call to discuss yesterday's email today.")
    elif (day > 9 and day < 16) or (day > 17 and day < 22) or (day > 23 and day < 30):
        callback.Call("The participant should receive an SMS message today.")
    elif day == 30:
        callback.Call("You should visit the participant today to retrieve the monitor - the intervention is finished.")
    elif day > 30:
        callback.Call("The monitor should have been collected. If it has not been, collect it as soon as possible.")
    else:
        callback.Call("Unknown error. Complete intervention manually using startdate.")
    callback.Call(True)

def callback_add_participant(participant_id, name, phone, email, address, postcode, foobot_id, startdate, callback):
    global parts
    global all_participants_data
    dlog("callback_add_participant")
    print "in callback_add_participant"
    filename = app_settings.participant_data_file
    try:
        startdatetime = datetime.datetime.strptime(startdate, "%Y-%m-%d")
        startdate = startdatetime.strftime("%d/%m/%Y")
    except:
        pass
    try:
        phone = str(phone).replace("+", "")
        phone = "'" + phone + "'"
        participants.append_participant_to_csv(filename, participant_id, name, phone, email, address, postcode, foobot_id, startdate, "", "")
        # parts = participants.read_participants_from_csv(app_settings.participant_data_file)
        # all_participants_data = get_all_data_for_all_participants()
    except Exception as e:
        dlog(str(e))
        callback.Call(False)
    callback.Call(True)

def callback_get_log(participant_id, callback):
    dlog("callback_get_log")
    log = comms_logging.get_log(participant_id, app_settings.comms_log_folder)
    callback.Call(log)


def set_synced_date(callback):
    dlog("set_synced_date")
    app_settings.last_sync = datetime.datetime.now()
    app_settings.write_settings()
    callback.Call(app_settings.last_sync.strftime("%d/%m/%y %H:%M"))


def get_synced_date(callback):
    dlog("get_synced_date")
    callback.Call(app_settings.last_sync.strftime("%d/%m/%y %H:%M"))


def get_settings(callback):
    dlog("get_settings")
    settings_dict = {"last_sync": app_settings.last_sync,
                     "participant_data_file": app_settings.participant_data_file,
                     "local_smokefree_home_pm": app_settings.local_smokefree_home_pm,
                     "username": app_settings.username,
                     "apikey": app_settings.apikey,
                     "your_city": app_settings.your_city,
                     "comms_log_folder": app_settings.comms_log_folder,}
    callback.Call(settings_dict)


def save_settings(participant_data_file, local_smokefree_home_pm, username, apikey, your_city, comms_log_folder, callback):
    dlog("save_settings")
    app_settings.participant_data_file = participant_data_file
    app_settings.local_smokefree_home_pm = float(local_smokefree_home_pm)
    app_settings.username = username
    app_settings.apikey = apikey
    app_settings.your_city = your_city
    app_settings.comms_log_folder = comms_log_folder
    try:
        app_settings.write_settings()
        callback.Call(True)
    except Exception as e:
        callback.Call(str(e))


def callback_email_content(part_id, seven_day_average, callback, graph_filename="", local_sfh=int(app_settings.local_smokefree_home_pm)):
    dlog("callback_email_content")
    viz = emails.get_average_viz(seven_day_average)
    dlog(viz)
    content = emails.get_email_content(part_id, seven_day_average, local_sfh, graph_filename, viz)
    filename = tempfile.mkstemp()
    print type(content)
    print type(filename[1])
    open_file = codecs.open(filename[1], 'wb', encoding="utf-8")
    open_file.write(content)
    callback.Call(filename[1])


def callback_email_send(part_id, email, seven_day_average, callback):
    dlog("callback_email_send")
    local_sfh=int(app_settings.local_smokefree_home_pm)
    print "make and send email"
    print "part_id: " + part_id
    graph_filename = return_graph_seven_days(part_id)
    resp = emails.make_and_send_email(part_id, email, seven_day_average, local_sfh, graph_filename)
    callback.Call(resp)


def callback_parse_log(participant_id, callback):
    dlog("callback_parse_log")
    log_data = comms_logging.get_log(participant_id, app_settings.comms_log_folder)
    parsed = comms_logging.parse_log(log_data)
    callback.Call(parsed)


def callback_count_last_day_records(participant_id, callback):
    dlog("callback_count_last_day_records")
    # records = data_storage.count_records_last_day(participant_id)
    records = len(all_participants_data[participant_id]["since_start_of_last_day"])
    callback.Call(records)


# def callback_export_data(callback):
#     dlog("callback_export_data")
#     filename = data_storage.export_all_local_data_csv()
#     callback.Call(filename)


def message_popup(text, die_after=False):
    dlog("message_popup")
    print "creating message box: ", text
    msgBox = QMessageBox()
    msgBox.setText(text)
    msgBox.exec_()
    if die_after:
        sys.exit()
    else:
        return True


def callback_file_chooser(callback):
    dlog("in file chooser")
    fname, second = QFileDialog.getOpenFileName(win, "Choose file", "~")
    dlog(fname)
    dlog(second)
    callback.Call(fname)


def callback_folder_chooser(callback):
    dlog("in folder chooser")
    fname = QFileDialog.getExistingDirectory(win)
    dlog(fname)
    callback.Call(fname)


def callback_update_property(callback):
    print "in callback_update_property"
    bindings = cefpython.JavascriptBindings(True)
    bindings.SetProperty("test_property", "changed in python")
    callback.Call(0)


class CefWidget(QWidget):
    browser = None
    def __init__(self, parent = None):
        super(CefWidget, self).__init__(parent)
        self.show()

    def embed(self):
        #it needs to be called after setupping the layout,  

        #Get current location
        loc = os.getcwd()
        interface_html = os.path.join(loc, "interface.html")
        # interface_html = "http://127.0.0.1:8080/interface.html"

        # try:
        #     self.name_uuid_dict = get_foobot_name_uuid_dict()
        #     dlog(self.name_uuid_dict)
        # except Exception as e:
        #     dlog(e)
        self.name_uuid_dict = {}

        windowInfo = cefpython.WindowInfo()
        windowInfo.SetAsChild(int(self.winIdFixed()))
        self.browser = cefpython.CreateBrowserSync(windowInfo,
                                                   browserSettings={},
                                                   navigateUrl=interface_html,
                                                   )
        bindings = cefpython.JavascriptBindings(True)
        bindings.SetFunction("send_text_message", send_text_message)
        # bindings.SetFunction("download_foobot_data", download_foobot_data)
        bindings.SetProperty("test_property", "Set in python")
        bindings.SetProperty("other", "other")
        bindings.SetProperty("participants", parts) # Parts is created in the ifmain
        bindings.SetProperty("get_parts", get_parts)
        # bindings.SetProperty("foobot_names", self.name_uuid_dict.keys())
        # bindings.SetProperty("download_all_participants_data", download_all_participants_data)
        bindings.SetProperty("get_last_day_mean", get_last_day_mean)
        bindings.SetProperty("get_seven_day_mean", get_seven_day_mean)
        bindings.SetProperty("get_local_smokefree_pm", get_local_smokefree_pm)
        bindings.SetProperty("get_sms_draft", get_sms_draft)
        bindings.SetProperty("get_graph_seven_days", get_graph_seven_days)
        bindings.SetProperty("is_selected", is_selected)
        bindings.SetProperty("set_synced_date", set_synced_date)
        bindings.SetProperty("get_synced_date", get_synced_date)
        bindings.SetProperty("save_settings", save_settings)
        bindings.SetProperty("get_settings", get_settings)
        bindings.SetProperty("callback_participant_day", callback_participant_day)
        bindings.SetProperty("callback_add_participant", callback_add_participant)
        bindings.SetProperty("callback_participant_required_today", callback_participant_required_today)
        bindings.SetProperty("callback_get_log", callback_get_log)
        bindings.SetProperty("callback_email_content", callback_email_content)
        bindings.SetProperty("callback_parse_log", callback_parse_log)
        bindings.SetProperty("callback_count_last_day_records", callback_count_last_day_records)
        # bindings.SetProperty("callback_export_data", callback_export_data)
        bindings.SetProperty("log_phone_call", log_phone_call)
        bindings.SetProperty("callback_email_send", callback_email_send)
        bindings.SetProperty("callback_file_chooser", callback_file_chooser)
        bindings.SetProperty("callback_folder_chooser", callback_folder_chooser)

        bindings.SetProperty("get_graph_since_start_of_intervention", get_graph_since_start_of_intervention)
        self.browser.SetJavascriptBindings(bindings)
        # print self.browser.ExecuteJavascript("alert('hello')")
        bindings = self.browser.GetJavascriptBindings()
        bindings.SetProperty("test_property", "now it's different")
        bindings.SetProperty("callback_update_property", callback_update_property)
        bindings.Rebind()


    def winIdFixed(self):
        # PySide bug: QWidget.winId() returns <PyCObject object at 0x02FD8788>,
        # there is no easy way to convert it to int.
        try:
            return int(self.winId())
        except:
            if sys.version_info[0] == 2:
                ctypes.pythonapi.PyCObject_AsVoidPtr.restype = ctypes.c_void_p
                ctypes.pythonapi.PyCObject_AsVoidPtr.argtypes = [ctypes.py_object]
                return ctypes.pythonapi.PyCObject_AsVoidPtr(self.winId())
            elif sys.version_info[0] == 3:
                ctypes.pythonapi.PyCapsule_GetPointer.restype = ctypes.c_void_p
                ctypes.pythonapi.PyCapsule_GetPointer.argtypes = [ctypes.py_object]
                return ctypes.pythonapi.PyCapsule_GetPointer(self.winId(), None)

    def moveEvent(self, event):
        cefpython.WindowUtils.OnSize(int(self.winIdFixed()), 0, 0, 0)

    def resizeEvent(self, event):
        cefpython.WindowUtils.OnSize(int(self.winIdFixed()), 0, 0, 0)



class MainWindow(QMainWindow):
    def __init__(self, parent = None):
        super(MainWindow, self).__init__(parent)
        self.setGeometry(150,150, 1000, 600)
        self.setWindowTitle("TaCklE - TackSHS WP4 Control Environment")

        self.view = CefWidget(self)
        
        # m_vbox = QVBoxLayout()
        # m_label = QLabel("Another Widget")
        # m_label.setMaximumHeight(100)
        
        m_vbox = QVBoxLayout()
        # m_vbox.addWidget(m_label)
        m_vbox.addWidget(self.view)
    
        #Do not use it
        #m_vbox.insertStretch(-1, 1)
        
        frame = QFrame()
        frame.setLayout(m_vbox)
        self.setCentralWidget(frame)

        #it needs to be called after setupping the layout 
        self.view.embed()

class CefApplication(QApplication):
    timer = None
    def __init__(self, args):
        super(CefApplication, self).__init__(args)
        self.createTimer()

    def createTimer(self):
        self.timer = QTimer()
        self.timer.timeout.connect(self.onTimer)
        self.timer.start(10)

    def onTimer(self):
        cefpython.MessageLoopWork()

    def stopTimer(self):
        # Stop the timer after Qt message loop ended, calls to MessageLoopWork()
        # should not happen anymore.
        self.timer.stop()


class JavascriptExternal:
    def __init__(self, browser):
        self.browser = browser

    def TestJSCallback(self, jsCallback):
        dlog("Callback " + str(jsCallback.GetFunctionName()))


def get_parts(callback):
    global parts
    try:
        callback.Call(parts)
    except:
        dlog("can't send parts")
        callback.Call(None)



def main():
    global all_participants_data
    global win
    # global app

    # set cwd - useful when installed
    dlog(os.getcwd())
    dlog(os.path.realpath(__file__))
    # script_dir = os.path.dirname(os.path.realpath(__file__))
    # dlog(script_dir)
    # os.chdir(script_dir)

    # cefpython/qt settings
    qtsettings = {}
    qtsettings["browser_subprocess_path"] = "%s/%s" % (
        cefpython.GetModuleDirectory(), "subprocess")
    qtsettings["context_menu"] = {
        "enabled": False,
        "navigation": False,  # Back, Forward, Reload
        "print": False,
        "view_source": False,
        "external_browser": False,  # Open in external browser
        "devtools": False,  # Developer Tools
    }

    cefpython.Initialize(qtsettings)

    app = CefApplication(sys.argv)
    win = MainWindow()

    pixmap = QPixmap("splash.png")
    splash = QSplashScreen(pixmap)
    splash.show()

    all_participants_data = get_all_data_for_all_participants()
    # print all_participants_data
    if all_participants_data == False:
        # print "Error - connection couldn't be established"
        # msgBox = QMessageBox()
        # msgBox.setText("Error - connection couldn't be established. No data could be downloaded. Check your internet connection and try again")
        # msgBox.exec_()
        # sys.exit()
        text = "Error - connection couldn't be established. No data could be downloaded. Check your internet connection and try again. If this persists even when you have a good connection, contact <a href='mailto:ruaraidh.dobson@abdn.ac.uk'>ruaraidh.dobson@abdn.ac.uk</a>"
        message_popup(text, die_after=True)
    dlog("all_participants_data")
    app_settings.last_sync = datetime.datetime.now()
    # print all_participants_data
    win.show()
    splash.finish(win)
    # print help(app)
    app.exec_()
    app.stopTimer()
    del win
    del app
    cefpython.Shutdown()


if __name__ == "__main__":
    main()
