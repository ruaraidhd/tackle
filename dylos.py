import requests
from datetime import datetime, timedelta

BASE_URL = "https://ruaraidhdobson.pythonanywhere.com/"
API_KEY = "727ed05f610be2cad5645262bb8604863dc12646ed70cd24a760d746144edea2"


def get_since_datetime(dylos_id, start_datetime):
    """Takes a datetime object representing a start date and sends a request
       for all data from then in json format."""
    json_url = BASE_URL + "json/"
    id_url = json_url + str(dylos_id) + "/"
    between_url = id_url + "since/"
    start = start_datetime.strftime("%Y-%m-%d/%H-%M")
    print "starting at " + start
    # now = datetime.now() + timedelta(days=1)  # Add one to make the API get data from up to the end of the day
    # now = now.strftime("%Y-%m-%d")
    # print now
    # dates_url = between_url + start + "/" + now + "/"
    dates_url = between_url + start
    req = requests.get(dates_url, headers={"X-Api-Key": API_KEY})
    print req
    data = req.json()
    return data


def get_on_day(dylos_id, day):
    """Takes a datetime object representing a day and requests all data from
       that day to 00:00 the next day."""
    json_url = BASE_URL + "json/"
    id_url = json_url + str(dylos_id) + "/"
    between_url = id_url + "between/"
    day_after = day + timedelta(days=1)
    day_after = day_after.strftime("%Y-%m-%d")
    day = day.strftime("%Y-%m-%d")
    dates_url = between_url + day + "/" + day_after + "/"
    req = requests.get(dates_url, headers={"X-Api-Key": API_KEY})
    print req
    data = req.json()
    return data


def get_records_last_seven_days(dylos_id):
    now = datetime.now()
    seven_days_ago = now - timedelta(days=7)
    data = get_since_datetime(dylos_id, seven_days_ago)
    return data


# def get_datetime_of_last_record_in_db(dylos_id):


def get_between_datetimes(dylos_id, startdate, enddate):
    json_url = BASE_URL + "json/"
    id_url = json_url + str(dylos_id) + "/"
    between_url = id_url + "between/"
    start = startdate.strftime("%Y-%m-%d")
    end = enddate + timedelta(days=1)  # Add one to make the API get data from up to the end of the day
    end = end.strftime("%Y-%m-%d")
    dates_url = between_url + start + "/" + end + "/"
    req = requests.get(dates_url, headers={"X-Api-Key": API_KEY})
    print req
    data = req.json()
    return data


def get_mean_of_data(data):
    """Convenience function, returns float arithmetic mean"""
    try:
        mean = sum([float(x["PM2.5 equivalent"]) for x in data]) / len(data)
        return mean
    except:
        print "in get_mean_of_data, error, making 0"
        return 0.0


def get_records_since_start_of_intervention_and_metadata(dylos_id, startdate, enddate=None):
    if type(startdate) == str:
        if "/" in startdate:
            startdate = datetime.strptime(startdate, "%d/%m/%Y")
        elif "-" in startdate:
            startdate = datetime.strptime(startdate, "%Y-%m-%d")
    if type(enddate) == str:
        if "/" in enddate:
            enddate = datetime.strptime(enddate, "%d/%m/%Y")
        elif "-" in enddate:
            enddate = datetime.strptime(enddate, "%Y-%m-%d")
    if enddate is not None:
        # get_between_datetimes()
        pass
        data = []
    else:
        data = get_since_datetime(dylos_id, startdate)
        print dylos_id
        print "data"
        print len(data)
    pm = [float(x["PM2.5 equivalent"]) for x in data]
    mean = sum(pm) / len(pm)
    print "mean"
    print mean

    # Get the actual last day's data, as in the last 24 hours'
    one_day_ago = datetime.now() - timedelta(days=1)
    print "one_day_ago: ", one_day_ago
    since_start_of_last_day = get_since_datetime(dylos_id, one_day_ago)
    for item in since_start_of_last_day:
        local_datetime = datetime.strptime(item["datetime"], "%d/%m/%y %H:%M")
        item["python_datetime"] = local_datetime
    last_24_hours = [x for x in since_start_of_last_day if x["python_datetime"] > one_day_ago]
    try:
        mean_last_24_hours = round(get_mean_of_data(last_24_hours))
    except:
        print "mean last 24 hours error, making it 0"
        mean_last_24_hours = 0

    for item in data:
        # print "item"
        # print item
        local_datetime = datetime.strptime(item["datetime"], "%d/%m/%y %H:%M")
        item["python_datetime"] = local_datetime
        item["date"] = local_datetime.date()
    print "len(data)"
    print len(data)
    dates_listed = list(set([x["python_datetime"].date() for x in data]))
    print "dates_listed"
    # print dates_listed
    by_date = {}
    for date in dates_listed:
        by_date[date.strftime("%d/%m/%Y")] = []
        print "by_date"
        # print by_date
    print "final created by_date"

    seven_days_ago = datetime.now() - timedelta(days=7)
    print "seven_days_ago: ", str(seven_days_ago)
    last_seven_days = [x for x in data if x["python_datetime"] > seven_days_ago]
    total = float(sum([x["PM2.5 equivalent"] for x in last_seven_days]))
    length = len(last_seven_days)
    mean_last_seven_days = round(total / length)

    # print by_date
    for item in data:
        by_date[item["date"].strftime("%d/%m/%Y")].append(item)
        # print "loaded by_date"
        # print by_date
    means_by_date = {}
    for key in by_date:
        mean = round(sum([x["PM2.5 equivalent"] for x in by_date[key]]) / len([x["PM2.5 equivalent"] for x in by_date[key]]))
        print "mean 2"
        # print mean
        means_by_date[key] = mean
        print "key"
        # print key
    count_of_dates = dates_listed[0] - dates_listed[-1]
    # print dates_listed[-1]
    # print dates_listed[0]
    # print count_of_dates
    # print int(count_of_dates.days) + 1
    by_index_of_days = []
    for key in by_date.keys():
        print key
        by_index_of_days.append(by_date[key])
        # print by_
    # # mean_last_seven_days = sum([x["PM2.5 equivalent"] for x in by_index_of_days[0:6]])/7
    # total = 0.0
    # length = 0
    # for x in by_index_of_days[0:6]:
    #     sum_of = sum([y["PM2.5 equivalent"] for y in x])
    #     total += sum_of
    #     length += len(x)
    # mean_last_seven_days = round(total / length)
    print "mean_last_seven_days"
    print mean_last_seven_days
    all_data = {"data": data,
                "dylos_id": dylos_id,
                "mean": mean,
                "startdate": startdate,
                "enddate": enddate,
                "by_date": by_date,
                "count_of_dates": int(count_of_dates.days) + 1,
                "by_index_of_days": by_index_of_days,
                "means_by_date": means_by_date,
                "mean_last_seven_days": mean_last_seven_days,
                "mean_last_24_hours": mean_last_24_hours,
                "since_start_of_last_day": since_start_of_last_day,
                "last_seven_days": last_seven_days,}
    print "all_data"
    print len(all_data)
    return all_data


def get_pm_from_data(data):
    return [x["PM2.5 equivalent"] for x in data]


def get_datetimes_from_data(data):
    return [x["python_datetime"] for x in data]


if __name__ == '__main__':
    # print datetime.strptime("01/05/2017", "%d/%m/%Y")
    # get_since_datetime(362, datetime.strptime("01/05/2017", "%d/%m/%Y"))
    # print get_on_day(362, datetime.strptime("12/05/2017", "%d/%m/%Y"))
    # # print get_records_last_seven_days("Sean_test_PT2")
    # start = datetime.strptime("15/05/2017", "%d/%m/%Y")
    # end = datetime.now() + timedelta(days=1)
    # data = get_records_since_start_of_intervention_and_metadata("FAKE_BLAST_1", start)
    # # pm = [float(x["PM2.5 equivalent"]) for x in data["data"]]
    # print data["means_by_date"]
    # # print sum(pm) / len(pm)
    x = get_records_since_start_of_intervention_and_metadata("362", "16/06/2017")
    print x["mean_last_seven_days"]
    print x["mean_last_24_hours"]
    print "length of last_seven_days: ", str(len(x["last_seven_days"]))

