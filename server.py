from SimpleHTTPServer import SimpleHTTPRequestHandler
from BaseHTTPServer import HTTPServer
import threading
import time
import logging
import os

ROUTES = [
    ('/', os.path.join(os.getcwd(), "interface.html")),
         ]

class TackSHSHandler(SimpleHTTPRequestHandler):
    def translate_path(self, path):
        root = os.getcwd()
        for patt, rootDir in ROUTES:
            if path.startswith(patt):
                path = path[len(patt):]
                root = rootDir
                break
        return os.path.join(root, path)

def create(port=8080):
    logger = logging.getLogger("tackshs")
    fh = logging.FileHandler("tackshs.log")
    server = HTTPServer(("", port), SimpleHTTPRequestHandler)
    logger.info("created server")
    thread = threading.Thread(target=server.serve_forever)
    logger.info("thread created")
    thread.daemon = True
    logger.info("thread.daemon is True")
    try:
        thread.start()
        logger.info("thread started, we're off to the races")
    except KeyboardInterrupt:
        server.shutdown()
        logger.info("something's gone wrong, returning None")
        return None
        # sys.exit(0)
    return thread
    # print "OK"
    # time.sleep(120)

if __name__ == '__main__':
    thread = create()
    time.sleep(20)
