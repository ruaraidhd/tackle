import foobot
from peewee import *
import datetime
import csv
import os
import json
import threading

db = SqliteDatabase("records.db")

# class Participant(Model):
#     name = CharField()
#     # address = CharField()
#     # phone_number = CharField()
#     # notes = CharField()
#     # monitorId = CharField()
#     # monitorInstalled = DateTimeField()
#     # monitorRetrieved = DateTimeField()
#     # consentForm = BooleanField()
#     class Meta:
#         database = db

class PM25Record(Model):
    participant = CharField()
    datetime = DateTimeField()
    PM25 = FloatField()
    foobot_id = CharField()
    class Meta:
        database = db
        indexes = (
                   (("participant", "foobot_id", "datetime"), True),
            )


def create_records_for_participant(participant_id, foobot_id, foobot_uuid, startdate, complete_integration=False, user=None, api=None):
    try:
        participant_records = PM25Record.select().where(PM25Record.participant == participant_id).get()
    except:
        participant_records = []
    print participant_records
    print startdate
    try:
        startdate_datetime = datetime.datetime.strptime(startdate, "%d/%m/%y")
    except ValueError as e:
        print e
        startdate_datetime = datetime.datetime.strptime(startdate, "%d/%m/%Y")
    enddate_datetime = startdate_datetime + datetime.timedelta(days=31)
    print startdate_datetime
    if user is not None and api is not None:
        data = foobot.get_data(foobot_uuid, foobot.days_to_seconds(30), user=user, api=api).json()
    else:
        data = foobot.get_data(foobot_uuid, foobot.days_to_seconds(30)).json()
    print "data downloaded"
    try:
        print data["datapoints"]
    except KeyError as e:
        print "couldn't find datapoints!"
        return False
    # for i in data["datapoints"]:
        # print type(i[0])
        # print datetime.datetime.fromtimestamp(i[0])
        # print type(startdate_datetime)
    time_checked_data = [x for x in data["datapoints"] if datetime.datetime.fromtimestamp(x[0]) > startdate_datetime and datetime.datetime.fromtimestamp(x[0]) < enddate_datetime]
    if complete_integration is False:
        print "carrying out incomplete integration"
        print len(time_checked_data)
        try:
            last_record = get_datetime_of_last_record_in_db(participant_id)
        except Exception as e:
            print e
            last_record = startdate_datetime
            try:
                db.connect()
                db.create_tables([PM25Record])
            except Exception as e:
                print "Does db already exist?"
                print e
        time_checked_data = [x for x in data["datapoints"] if datetime.datetime.fromtimestamp(x[0]) > last_record]
        print len(time_checked_data)
    # new_records = []
    with db.atomic():
        for record in time_checked_data:
            try:
                new_record = PM25Record.create(participant=participant_id, datetime=datetime.datetime.fromtimestamp(record[0]), PM25=float(record[1]), foobot_id=foobot_id)
                new_record.save()
            except IntegrityError as e:
                print e
    return True
        # for record in new_records:


def get_datetime_of_last_record_in_db(participant_id):
    record = PM25Record.select().where(PM25Record.participant == participant_id).order_by(-PM25Record.datetime).get()
    return record.datetime

def get_records_last_day(participant_id):
    records = PM25Record.select().where(PM25Record.participant == participant_id, PM25Record.datetime > datetime.datetime.now() - datetime.timedelta(days=1)).order_by(PM25Record.datetime)
    return records


def count_records_last_day(participant_id):
    records = get_records_last_day(participant_id)
    return len(records)


def get_records_last_seven_days(participant_id):
    records = PM25Record.select().where(PM25Record.participant == participant_id, PM25Record.datetime > datetime.datetime.now() - datetime.timedelta(days=7)).order_by(PM25Record.datetime)
    print "getting records"
    print participant_id
    # print records[0].participant
    try:
        if records[0].participant != participant_id:
            print "they're not equal"
    except Exception as e:
        print e
    return records


def export_participant_local_data_csv(participant_id):
    records = PM25Record.select().where(PM25Record.participant == participant_id).order_by(PM25Record.datetime)
    tup_records = [(x.participant, x.datetime.strftime("%d/%m/%y %H:%M"), x.PM25, x.foobot_id) for x in records]
    filename = "export_" + participant_id + "_" + datetime.datetime.now().strftime("%y%m%d_%H%M%S") + ".csv"
    with open(filename, 'wb') as open_file:
        writer = csv.writer(open_file)
        writer.writerow(("Participant ID", "Datetime", "PM2.5 (ug/m3)", "Foobot ID"))
        for row in tup_records:
            writer.writerow(row)
    return filename


def export_all_local_data_csv():
    records = PM25Record.select()
    tup_records = [(x.participant, x.datetime.strftime("%d/%m/%y %H:%M"), x.PM25, x.foobot_id) for x in records]
    filename = "export_all_" + datetime.datetime.now().strftime("%y%m%d_%H%M%S") + ".csv"
    with open(filename, 'wb') as open_file:
        writer = csv.writer(open_file)
        writer.writerow(("Participant ID", "Datetime", "PM2.5 (ug/m3)", "Foobot ID"))
        for row in tup_records:
            writer.writerow(row)
    return os.path.abspath(filename)

def get_mean_pm_last_day(participant_id):
    records = get_records_last_day(participant_id)
    try:
        mean = round(sum([x.PM25 for x in records])/float(len(records)),0)
        return mean
    except ZeroDivisionError as e:
        print e
        return 0


def get_mean_pm_last_seven_days(participant_id):
    print "last seven days"
    print participant_id
    records = get_records_last_seven_days(participant_id)
    print len(records)
    try:
        mean = round(sum([x.PM25 for x in records])/float(len(records)), 0)
        return mean
    except ZeroDivisionError as e:
        print e
        return 0

def make_fake_record(participant_id, datetime_object, pm25, foobot_id):
    fake_record = PM25Record.create(participant=participant_id, datetime=datetime_object, PM25=pm25, foobot_id=foobot_id)
    fake_record.save()

if __name__ == '__main__':
    # db.connect()
    # db.create_tables([PM25Record])
    # create_records_for_participant("WP4_SCO_01", "TACKSHS_1", "25004667617040D0", "14/1/17")
    # for i in get_records_last_seven_days("WP4_SCO_01"):
    #     print i.datetime
    # make_fake_record("WP4_SCO_01", datetime.datetime.now(), random.randint(0,100), "FAKE_FOOBOT")
    # print get_mean_pm_last_day("WP4_SCO_01")
    # rec = get_datetime_of_last_record_in_db("WP4_SCO_01")
    # print rec
    # print type(rec)
    # export_participant_local_data_csv("WP4_SCO_01")
    export_all_local_data_csv()
